import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import App from './App';
import HomePage from '../pages/HomePage';
import PackageReleasesPage from '../pages/PackageReleasesPage';
import BuildStatsPage from '../pages/BuildStatsPage';
import PackageStatsPage from '../pages/PackageStatsPage';
import DependencyGraphsPage from '../pages/DependencyGraphsPage';

export default class MainRouter extends PureComponent {
  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 320,
      }
    }
  }

  getChildContext() {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  appWithPersistentNav = () => (props) => (
    <App
      onNavResize={this.onNavResize}
      {...props}
    />
  )

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,
    });
  }

  render() {
    return (
      <Router history={browserHistory}>
        <Route component={this.appWithPersistentNav()}>
          <Route path="/" component={PackageReleasesPage} />
          <Route path="/package-releases" component={PackageReleasesPage} />
          <Route path="/build-stats" component={BuildStatsPage} />
          <Route path="/package-stats" component={PackageStatsPage} />
          <Route path="/dependency-graphs" component={DependencyGraphsPage} />
        </Route>
      </Router>
    );
  }
}

MainRouter.childContextTypes = {
  navOpenState: PropTypes.object,
};