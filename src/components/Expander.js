// @flow

import React, { Component } from 'react';
import styled from 'styled-components';
import Button from '@atlaskit/button';
import ChevronDownIcon from '@atlaskit/icon/glyph/chevron-down';
import ChevronUpIcon from '@atlaskit/icon/glyph/chevron-up';

type Props = {
  defaultOpen: boolean,
  children: Function,
}

type State = {
  isOpen: boolean
};

const OuterContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Container = styled.span`
  display: inline-block;
  white-space: ${({ isOpen }) => (isOpen ? 'initial' : 'nowrap')};
  text-overflow: ellipsis;
  overflow: hidden;
  width: calc(100% - 48px);
`;

export default class Expander extends Component<Props, State> {
  static defaultProps = {
    defaultOpen: false,
  }

  constructor(props) {
    super(props);
    this.state = {
      isOpen: props.defaultOpen,
    };
  }

  handleExpandClick = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {
    const { isOpen } = this.state;
    return (
      <OuterContainer>
        <Container isOpen={isOpen}>
          {this.props.children({ isOpen })}
        </Container>
        <Button apperance="subtle" onClick={this.handleExpandClick}>
          {isOpen ? <ChevronUpIcon /> : <ChevronDownIcon />}
        </Button>
      </OuterContainer>
    )
  }
}