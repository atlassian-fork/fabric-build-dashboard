import React, { Component } from 'react';
import ContentWrapper from '../components/ContentWrapper';

export default class HomePage extends Component {
  render() {
    return (
      <ContentWrapper>
        <h1>Package Stats</h1>
        <p>Get all your fabric build information here.</p>
      </ContentWrapper>
    );
  }
}