import React, { Component } from 'react';
import ContentWrapper from '../components/ContentWrapper';

export default class HomePage extends Component {
  render() {
    return (
      <ContentWrapper>
        <h1>Dependency Graphs</h1>
      </ContentWrapper>
    );
  }
}